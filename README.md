# bcdclock

Arduino based BCD clock with calendar, supports any of PCF8523, PCF8563, DS1307, DS3231, or clock without external RTC.

**Components:**
- Arduino Nano
- 3 pcs. 6x6x8 mm micro push button switch
- Sensor button TTP223 
- 21 pcs. RGB LED NeoPixel WS2811 5 mm, 5 V
- PCF8523, PCF8563, DS1307 or DS3231 module (optionaly)

**Libraries:**
- [RTClib](https://github.com/adafruit/RTClib)
- [Adafruit_NeoPixel](https://github.com/adafruit/Adafruit_NeoPixel)

**Schematic:**

![](imgs/bcdclock.png)

![](imgs/1.jpg)
