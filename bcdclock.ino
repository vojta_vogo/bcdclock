#include <RTClib.h>
#include <Adafruit_NeoPixel.h>

// Uncomment used RTC or leave all commented out if your clock does not have any
//#define _RTC_PCF8523_
//#define _RTC_PCF8563_
//#define _RTC_DS1307_
//#define _RTC_DS3231_

#define DIAGONAL_HUE_OFFSET 1024

#define BTN_PLUS_PIN 2
#define BTN_SET_PIN 3
#define BTN_MINUS_PIN 4
#define BTN_MODE_PIN 5
#define NEOPIXEL_PIN 6

#define BUTTON_PUSH_MILLIS 50
#define BUTTON_REPEAT_START_MILLIS 500
#define BUTTON_REPEAT_MILLIS 150

class Button
{
public:
    Button(uint8_t pin, bool inverted)
        : buttonPin(pin), invertedInput(inverted)
    {
    }

    void begin()
    {
        pinMode(buttonPin, INPUT_PULLUP);
    }

    virtual void refresh()
    {
        prevPushed = pushed;
        pushed = digitalRead(buttonPin);

        if (invertedInput)
        {
            pushed = !pushed;
        }

        if (!prevPushed && pushed)
        {
            pushMillis = millis();
        }
    }

    bool isPushed()
    {
        unsigned long currentMillis = millis();

        if (pushed != prevPushed)
        {
            return !pushed && ((currentMillis - pushMillis) > BUTTON_PUSH_MILLIS) &&
                   ((currentMillis - pushMillis) < BUTTON_REPEAT_START_MILLIS);
        }

        if (pushed)
        {
            return ((currentMillis - pushMillis) >= BUTTON_REPEAT_START_MILLIS) &&
                   (((currentMillis - (pushMillis + BUTTON_REPEAT_START_MILLIS)) % BUTTON_REPEAT_MILLIS) == 0);
        }

        return false;
    }

public:
    uint8_t buttonPin = 0;
    bool invertedInput = false;
    bool pushed = false;
    bool prevPushed = false;
    unsigned long pushMillis = 0;
};

uint8_t decToBcd(uint8_t val)
{
    return val + (6 * (val / 10));
}

uint32_t getDisplay(uint8_t segment2, uint8_t segment1, uint8_t segment0)
{
    return (uint32_t)((uint32_t)((uint32_t)(decToBcd(segment2) & 0b111111) << 15) |
                      (uint32_t)((uint32_t)(decToBcd(segment1) & 0b1111111) << 8) |
                      (uint32_t)((uint32_t)(decToBcd(segment0))));
}

bool isLeapYear(uint16_t year)
{
    if (year % 4)
    {
        return false;
    }

    return ((year % 100) || !(year % 400));
}

uint8_t getMaxDay(uint16_t year, uint8_t month)
{
    switch (month)
    {
    case 1:
    case 3:
    case 5:
    case 7:
    case 8:
    case 10:
    case 12:
        return 31;
        break;
    case 2:
        return isLeapYear(year) ? 29 : 28;
        break;
    default:
        return 30;
        break;
    }
}

enum eMode : uint8_t
{
    SHOW_TIME,
    SHOW_DATE,
    SET_YEAR,
    SET_MONTH,
    SET_DAY,
    SET_HOUR,
    SET_MINUTE,
    SET_SECOND,
};

Button btnMode(BTN_MODE_PIN, false);
Button btnPlus(BTN_PLUS_PIN, true);
Button btnSet(BTN_SET_PIN, true);
Button btnMinus(BTN_MINUS_PIN, true);
Adafruit_NeoPixel neoPixel = Adafruit_NeoPixel(21, NEOPIXEL_PIN, NEO_RGB + NEO_KHZ800);

#ifdef _RTC_PCF8523_
RTC_DS3231 rtc;
#endif
#ifdef _RTC_PCF8563_
RTC_PCF8563 rtc;
#endif
#ifdef _RTC_DS1307_
RTC_DS1307 rtc;
#endif
#ifdef _RTC_DS3231_
RTC_DS3231 rtc;
#endif

eMode mode = SHOW_TIME;
DateTime now(__DATE__, __TIME__);
uint8_t yearToSet = 0, monthToSet = 0, dayToSet = 0, hourToSet = 0, minuteToSet = 0, secondToSet = 0;
bool lostPower = true;
uint8_t brightness = 50;
uint16_t basePixelHue = 0;
bool refresh = false;
bool blink = false;
unsigned long blinkMillis = 0;
unsigned long hueMillis = 0;
unsigned long showDateMillis = 0;
unsigned long clockMillis = 0;

void setup()
{
    btnMode.begin();
    btnPlus.begin();
    btnSet.begin();
    btnMinus.begin();

    neoPixel.begin();
    neoPixel.setBrightness(brightness);

#if defined(_RTC_PCF8523_) || defined(_RTC_PCF8563_) || defined(_RTC_DS1307_) || defined(_RTC_DS3231_)
    rtc.begin();
#ifdef _RTC_DS1307_
    lostPower = false;
#else
    lostPower = rtc.lostPower();
#endif
    if (!lostPower)
    {
        now = rtc.now();
    }
#endif
}

void loop()
{
    unsigned long currentMillis = millis();

    if ((currentMillis - blinkMillis) >= 500)
    {
        blinkMillis = currentMillis;
        blink = !blink;
        refresh = true;
    }

    if ((mode == SHOW_DATE) && ((currentMillis - showDateMillis) > 10000))
    {
        mode = SHOW_TIME;
        refresh = true;
    }

    if ((currentMillis - hueMillis) >= 100)
    {
        basePixelHue++;
        refresh = true;
    }

    if (((mode == SHOW_TIME) || (mode == SHOW_DATE)) && (currentMillis - clockMillis) >= 1000)
    {
        clockMillis = currentMillis;
#if defined(_RTC_PCF8523_) || defined(_RTC_PCF8563_) || defined(_RTC_DS1307_) || defined(_RTC_DS3231_)
        now = rtc.now();
#else
        now = now + TimeSpan(1);
#endif
        refresh = true;
    }

    if (refresh)
    {
        refresh = false;

        uint32_t display = 0;
        uint32_t mask = lostPower ? 0b111111111111111111111 : 0b000000000000000000000;

        switch (mode)
        {
        default:
        case SHOW_TIME:
            display = getDisplay(now.hour(), now.minute(), now.second());
            break;

        case SHOW_DATE:
            display = getDisplay(now.day(), now.month(), now.year() % 100);
            break;

        case SET_YEAR:
            display = getDisplay(dayToSet, monthToSet, yearToSet);
            mask = 0b000000000000011111111;
            break;

        case SET_MONTH:
            display = getDisplay(dayToSet, monthToSet, yearToSet);
            mask = 0b000000111111100000000;
            break;

        case SET_DAY:
            display = getDisplay(dayToSet, monthToSet, yearToSet);
            mask = 0b111111000000000000000;
            break;

        case SET_HOUR:
            display = getDisplay(hourToSet, minuteToSet, secondToSet);
            mask = 0b111111000000000000000;
            break;

        case SET_MINUTE:
            display = getDisplay(hourToSet, minuteToSet, secondToSet);
            mask = 0b000000111111100000000;
            break;

        case SET_SECOND:
            display = getDisplay(hourToSet, minuteToSet, secondToSet);
            mask = 0b000000000000011111111;
            break;
        }

        if (blink)
        {
            display |= mask;
        }

        uint32_t pixelHue = basePixelHue;
        for (uint8_t i = 0; i < 22; i++)
        {
            switch (i)
            {
            case 1:
            case 4:
                pixelHue = basePixelHue + DIAGONAL_HUE_OFFSET;
                break;
            case 2:
            case 5:
            case 8:
                pixelHue = basePixelHue + (2 * DIAGONAL_HUE_OFFSET);
                break;
            case 3:
            case 6:
            case 9:
            case 12:
                pixelHue = basePixelHue + (3 * DIAGONAL_HUE_OFFSET);
                break;
            case 7:
            case 10:
            case 13:
            case 15:
                pixelHue = basePixelHue + (4 * DIAGONAL_HUE_OFFSET);
                break;
            case 11:
            case 14:
            case 16:
            case 19:
                pixelHue = basePixelHue + (5 * DIAGONAL_HUE_OFFSET);
                break;
            case 17:
            case 20:
                pixelHue = basePixelHue + (6 * DIAGONAL_HUE_OFFSET);
                break;
            case 21:
                pixelHue = basePixelHue + (7 * DIAGONAL_HUE_OFFSET);
                break;
            default:
                break;
            }

            neoPixel.setPixelColor(i, bitRead(display, i) ? neoPixel.gamma32(neoPixel.ColorHSV(pixelHue)) : 0);
        }

        neoPixel.show();
    }

    btnMode.refresh();
    btnSet.refresh();
    btnPlus.refresh();
    btnMinus.refresh();

    if (btnMode.isPushed())
    {
        switch (mode)
        {
        default:
            mode = SHOW_TIME;
            break;

        case SHOW_TIME:
            mode = SHOW_DATE;
            showDateMillis = currentMillis;
            break;

        case SET_YEAR:
        case SET_MONTH:
        case SET_DAY:
        case SET_HOUR:
        case SET_MINUTE:
        case SET_SECOND:
            break;
        }

        refresh = true;
    }

    if (btnSet.isPushed())
    {
        switch (mode)
        {
        default:
            yearToSet = now.year() % 100;
            monthToSet = now.month();
            dayToSet = now.day();
            hourToSet = now.hour();
            minuteToSet = now.minute();
            secondToSet = now.second();
            mode = SET_YEAR;
            break;

        case SET_YEAR:
            mode = SET_MONTH;
            break;

        case SET_MONTH:
            mode = SET_DAY;
            break;

        case SET_DAY:
            mode = SET_HOUR;
            break;

        case SET_HOUR:
            mode = SET_MINUTE;
            break;

        case SET_MINUTE:
            mode = SET_SECOND;
            break;

        case SET_SECOND:
            now = DateTime(yearToSet, monthToSet, dayToSet, hourToSet, minuteToSet, secondToSet);
#if defined(_RTC_PCF8523_) || defined(_RTC_PCF8563_) || defined(_RTC_DS1307_) || defined(_RTC_DS3231_)
            rtc.adjust(now);
#endif
            clockMillis = currentMillis;
            lostPower = false;
            mode = SHOW_TIME;
            break;
        }

        refresh = true;
    }

    if (btnPlus.isPushed())
    {
        switch (mode)
        {
        case SET_YEAR:
            if (yearToSet < 99)
            {
                yearToSet++;
            }
            else
            {
                yearToSet = 0;
            }
            break;

        case SET_MONTH:
            if (monthToSet < 12)
            {
                monthToSet++;
            }
            else
            {
                monthToSet = 1;
            }
            break;

        case SET_DAY:
            if (dayToSet < getMaxDay(yearToSet, monthToSet))
            {
                dayToSet++;
            }
            else
            {
                dayToSet = 1;
            }
            break;

        case SET_HOUR:
            if (hourToSet < 23)
            {
                hourToSet++;
            }
            else
            {
                hourToSet = 0;
            }
            break;

        case SET_MINUTE:
            if (minuteToSet < 59)
            {
                minuteToSet++;
            }
            else
            {
                minuteToSet = 0;
            }
            break;

        case SET_SECOND:
            if (secondToSet < 59)
            {
                secondToSet++;
            }
            else
            {
                secondToSet = 0;
            }
            break;

        default:
            if (brightness <= 250)
            {
                brightness += 5;
                neoPixel.setBrightness(brightness);
            }
            break;
        }

        blinkMillis = currentMillis;
        blink = false;
        refresh = true;
    }

    if (btnMinus.isPushed())
    {
        switch (mode)
        {
        case SET_YEAR:
            if (yearToSet > 0)
            {
                yearToSet--;
            }
            else
            {
                yearToSet = 99;
            }
            break;

        case SET_MONTH:
            if (monthToSet > 1)
            {
                monthToSet--;
            }
            else
            {
                monthToSet = 12;
            }
            break;

        case SET_DAY:
            if (dayToSet > 1)
            {
                dayToSet--;
            }
            else
            {
                dayToSet = getMaxDay(yearToSet, monthToSet);
            }
            break;

        case SET_HOUR:
            if (hourToSet > 0)
            {
                hourToSet--;
            }
            else
            {
                hourToSet = 23;
            }
            break;

        case SET_MINUTE:
            if (minuteToSet > 0)
            {
                minuteToSet--;
            }
            else
            {
                minuteToSet = 59;
            }
            break;

        case SET_SECOND:
            if (secondToSet > 0)
            {
                secondToSet--;
            }
            else
            {
                secondToSet = 59;
            }
            break;

        default:
            if (brightness > 10)
            {
                brightness -= 5;
                neoPixel.setBrightness(brightness);
            }
            break;
        }

        blinkMillis = currentMillis;
        blink = false;
        refresh = true;
    }
}
